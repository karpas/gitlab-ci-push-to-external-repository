# gitlab-ci-push-to-external-repository

Example how to push branch from GitLab repository to external repository using Gitlab CI.

# Usage

## Variables
Add variables to your [Gitlab CI configuration](https://gitlab.com/help/ci/variables/README#variables)
* $EXTERNAL_REPOSITORY_USERNAME - username for external git repository
* $EXTERNAL_AUTHENTICATION - password or authentication token
* $EXTERNAL_REPOSITORY_REPOSITORY - url to external repository

## Specify branch
Current example will fire only on master branch update, then it will fetch current master and push to master branch at external repository.